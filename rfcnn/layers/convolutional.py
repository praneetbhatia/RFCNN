import numpy
import theano
from theano.tensor.nnet import conv2d
import theano.tensor as T
from theano import config

import pdb

from ..utils.generic import numpy_floatX

class Conv(object):

    """Docstring for convolution. """

    def __init__(self, name, filter_size, stride, num_in, num_out, padding=0, lr_adj=1.0):
        """TODO: to be defined1. """
        self.rng = numpy.random.RandomState(23455)
        self.name = name
        self.filter_size = filter_size
        self.stride = stride
        self.num_in = num_in
        self.num_out = num_out
        self.padding = padding
        self.lr_adj = numpy_floatX(lr_adj)
        self.params = {}
        self._set_weights(name, filter_size, num_in, num_out)



    def _set_weights(self, name, filter_size, num_in, num_out):
        """TODO: Docstring for set_weights.
        :returns: TODO
        """
        w_shp = (num_out, num_in, filter_size, filter_size)
        w_bound = numpy.sqrt(num_in*filter_size**2)
        self.params[name+'_W'] = theano.shared( numpy.asarray(
                   self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                   dtype=theano.config.floatX), name = name + '_W')
        b_shp = (num_out,)
        self.params[name+'_b'] = theano.shared(numpy.asarray(
                   self.rng.uniform(low=-.5, high=.5, size=b_shp),
                   dtype=theano.config.floatX), name =name + '_b')

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        pad = self.padding
        padded_input= T.zeros((layer_input.shape[0],
                            layer_input.shape[1],
                            layer_input.shape[2]+2*pad,
                            layer_input.shape[3]+2*pad),dtype=theano.config.floatX)
        padded_input= T.set_subtensor(padded_input[:,:,pad:layer_input.shape[2]+pad, pad:layer_input.shape[3]+pad], layer_input)
        # convolve input feature maps with filters
        return conv2d(padded_input,
                        self.params[self.name+'_W'],
                        subsample=(self.stride,self.stride)) +\
                                self.params[self.name+'_b'].dimshuffle('x', 0, 'x', 'x')

    def get_shape(self, input_shape):
        """
        TODO
        """
        return [self.num_out,
                (input_shape[1] - self.filter_size+ 2*self.padding)/self.stride + 1,
                (input_shape[2] - self.filter_size+ 2*self.padding)/self.stride + 1]

class DilatedConv(object):

    """Docstring for convolution. """

    def __init__(self, name, filter_size, stride, num_in, num_out, dilation_factor, padding=0, lr_adj=1.0):
        """TODO: to be defined1. """
        self.rng = numpy.random.RandomState(23455)
        self.name = name
        self.filter_size = filter_size
        self.stride = stride
        self.num_in = num_in
        self.num_out = num_out
        self.padding = padding
        self.lr_adj = numpy_floatX(lr_adj)
        self.params = {}
        self._set_weights(name, filter_size, num_in, num_out)
        self.dil_factor= dilation_factor

    def _set_weights(self, name, filter_size, num_in, num_out):
        """TODO: Docstring for set_weights.
        :returns: TODO
        """
        w_shp = (num_out, num_in, filter_size, filter_size)
        w_bound = numpy.sqrt(num_in*filter_size**2)
        self.params[name+'_W'] = theano.shared( numpy.asarray(
                   self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                   dtype=theano.config.floatX), name = name + '_W')
        b_shp = (num_out,)
        self.params[name+'_b'] = theano.shared(numpy.asarray(
                   self.rng.uniform(low=-.5, high=.5, size=b_shp),
                   dtype=theano.config.floatX), name =name + '_b')

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        pad = self.padding
        padded_input= T.zeros((layer_input.shape[0],
                            layer_input.shape[1],
                            layer_input.shape[2]+2*pad,
                            layer_input.shape[3]+2*pad),dtype=theano.config.floatX)
        padded_input= T.set_subtensor(padded_input[:,:,pad:layer_input.shape[2]+pad, pad:layer_input.shape[3]+pad], layer_input)
        # convolve input feature maps with filters
        return conv2d(padded_input,
                        self.params[self.name+'_W'],
                        None, None,
                        border_mode= 'valid',
                        subsample= (self.stride,self.stride),
                        filter_dilation= (self.dil_factor, self.dil_factor)) +\
                                self.params[self.name+'_b'].dimshuffle('x', 0, 'x', 'x')

    def get_shape(self, input_shape):
        """
        TODO
        """
        dil_fsize= (self.filter_size-1)*self.dil_factor+1
        return [self.num_out,
                (input_shape[1] - dil_fsize+ 2*self.padding)/self.stride + 1,
                (input_shape[2] - dil_fsize+ 2*self.padding)/self.stride + 1]


class Deconv(object):

    """Docstring for convolution. """

    def __init__(self, name, filter_size, stride, num_in, num_out, lr_adj=1.0, out_crop_size=None):
        """TODO: to be defined1. """
        self.rng = numpy.random.RandomState(23455)
        self.name = name
        self.filter_size = filter_size
        self.stride = stride
        self.num_in = num_in
        self.num_out = num_out
        self.out_crop_size = out_crop_size
        self.lr_adj = numpy_floatX(lr_adj)
        self.params = {}
        self._set_weights(name, filter_size, num_in, num_out)

    def _set_weights(self, name, fsize, nin, nout):
        """TODO: Docstring for set_weights.
        :returns: TODO
        """
        def upsample_filt(size):
            size= float(size)
            factor = (size + 1) // 2
            #print('factor is ', factor)
            if size % 2 == 1:
                center = factor - 1
            else:
                center = factor - 0.5
            #print('center is ', center)
            og = numpy.ogrid[:size, :size]
            return (1 - abs(og[0] - center) / factor) * (1 - abs(og[1] - center) / factor)

        init_w= numpy.tile(upsample_filt(fsize), (nout, nin, 1, 1))
#            init_w= init_w/ init_w.sum()
        ########## I don't want to differentiate w.r.t the weights of deconv and not to change it
        self.deconv_w = theano.shared( numpy.asarray(
            init_w,
            dtype=theano.config.floatX), name ='deconv_w')
        self.deconv_w = self.deconv_w / T.sum(self.deconv_w)

        b_shp = (nout,)
        self.params[name+'_b'] = theano.shared(numpy.asarray(
            self.rng.uniform(low=-.5, high=.5, size=b_shp),
            dtype=theano.config.floatX), name =name +'_b')

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        fsize= self.filter_size
        nin = self.num_in
        nout = self.num_out
        stride = self.stride
        output = self.out_crop_size
        #1- Flatten the input
#            l_= layer_input.flatten()

        ##2- replicate with number stride*stride
        upsample= T.tile(layer_input.flatten(), (stride*stride,1)).T

        ##3-reshape to have n*stride*stride so that each element from 1..n is a small stride*stride rectangle with the repeated vals
        upsample= upsample.reshape((layer_input.shape[0],
            layer_input.shape[1],
            layer_input.shape[2]*layer_input.shape[3],
            stride,
            stride))

        ##4- Flip the dimensions so that it's stride*n*stride
        upsample= upsample.transpose((0,1,3,2,4))

        ##5- this will flatten the first axis only, -1 lets it figure out the rest of dims, thus we end with stride*(n*stride) array with prev. small rectangles patched together spatially
        upsample= upsample.reshape((upsample.shape[0],upsample.shape[1], upsample.shape[2], -1))

        ##6- Reshape to have stride*width*(height*stride)
        upsample= upsample.reshape((upsample.shape[0],
            upsample.shape[1],
            stride,
            layer_input.shape[2],
            layer_input.shape[3]*stride))

        ##7- Transpose dimensions to have (height*stride) dim first so it's flattened
        upsample= upsample.transpose(0,1,4,3,2)
        upsample= upsample.reshape((upsample.shape[0],
            upsample.shape[1],
            layer_input.shape[3]*stride,-1))

        ##8- Transpose purpose is just to transpose width and height of input
        upsample= upsample.transpose(0,1,3,2)

        # Apply Deconvolution, and add bias term
        layer_input = conv2d(upsample, self.deconv_w, border_mode='full') +\
                                self.params[self.name+'_b'].dimshuffle('x', 0, 'x', 'x')

        # Cropping according to the shape of the output (which should be the label)
        if output is None:
            return layer_input

        return  layer_input[:, :, :output[0],:output[1]]

    def get_shape(self, input_shape):
        """
        TODO
        """
        return [self.num_out,
                (self.stride*input_shape[1] - self.filter_size + 1),
                (self.stride*input_shape[2] - self.filter_size + 1)]


class TransDeconv(object):

    """Docstring for convolution. """

    def __init__(self, name, filter_size, stride, num_in, num_out, padding=0, lr_adj=1.0, out_crop_size=None):
        """TODO: to be defined1. """
        self.rng = numpy.random.RandomState(23455)
        self.name = name
        self.filter_size = filter_size
        self.stride = stride
        self.num_in = num_in
        self.num_out = num_out
        self.out_crop_size = out_crop_size
        self.lr_adj = numpy_floatX(lr_adj)
        self.params = {}
        self._set_weights(name, filter_size, num_in, num_out)
        self.padding= padding

    def _set_weights(self, name, filter_size, num_in, num_out):
        """TODO: Docstring for set_weights.
        :returns: TODO
        """
        w_shp = (num_in, num_out, filter_size, filter_size)
        w_bound = numpy.sqrt(num_in*filter_size**2)
        self.params[name+'_W'] = theano.shared( numpy.asarray(
                   self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                   dtype=theano.config.floatX), name = name + '_W')
        b_shp = (num_out,)
        self.params[name+'_b'] = theano.shared(numpy.asarray(
                   self.rng.uniform(low=-.5, high=.5, size=b_shp),
                   dtype=theano.config.floatX), name =name + '_b')


    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        out_shape = [None] + self.output_shape
        pad = self.padding
        padded_input= T.zeros((layer_input.shape[0],
                            layer_input.shape[1],
                            layer_input.shape[2]+2*pad,
                            layer_input.shape[3]+2*pad),dtype=theano.config.floatX)
        padded_input= T.set_subtensor(padded_input[:,:,pad:layer_input.shape[2]+pad, pad:layer_input.shape[3]+pad], layer_input)

        op = T.nnet.abstract_conv.AbstractConv2d_gradInputs(
            imshp=out_shape,
            kshp=self.params[self.name+'_W'].get_value().shape,
            subsample=(self.stride,self.stride),
            border_mode='full',
            filter_flip=True)
        output_size = out_shape[2:]
        layer_input = op(self.params[self.name+'_W'], padded_input, output_size) + \
                         self.params[self.name+'_b'].dimshuffle('x', 0, 'x', 'x')


        if self.out_crop_size is None:
            return layer_input

        return layer_input[:, :, :self.out_crop_size[0], :self.out_crop_size[1]]

    def get_shape(self, input_shape):
        """
        TODO
        """
        self.output_shape =[self.num_out,
                (self.stride*(input_shape[1]+self.padding*2) - self.filter_size + 1),
                (self.stride*(input_shape[2]+self.padding*2) - self.filter_size + 1)]
        return self.output_shape

class RectConv(object):

    """Docstring for convolution. """

    def __init__(self, name, filter_size, stride, num_in, num_out, padding=0, lr_adj=1.0):
        """TODO: to be defined1. """
        self.rng = numpy.random.RandomState(23455)
        self.name = name
        self.filter_size = filter_size
        self.stride = stride
        self.num_in = num_in
        self.num_out = num_out
        self.padding = padding
        self.lr_adj = numpy_floatX(lr_adj)
        self.params = {}
        self._set_weights(name, filter_size, num_in, num_out)



    def _set_weights(self, name, filter_size, num_in, num_out):
        """TODO: Docstring for set_weights.
        :returns: TODO
        """
        w_shp = (num_out, num_in, filter_size[0], filter_size[1])
        w_bound = numpy.sqrt(num_in*filter_size[0]**2)
        self.params[name+'_W'] = theano.shared( numpy.asarray(
                   self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                   dtype=theano.config.floatX), name = name + '_W')
        b_shp = (num_out,)
        self.params[name+'_b'] = theano.shared(numpy.asarray(
                   self.rng.uniform(low=-.5, high=.5, size=b_shp),
                   dtype=theano.config.floatX), name =name + '_b')

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        pad = self.padding
        padded_input= T.zeros((layer_input.shape[0],
                            layer_input.shape[1],
                            layer_input.shape[2]+2*pad,
                            layer_input.shape[3]+2*pad),dtype=theano.config.floatX)
        padded_input= T.set_subtensor(padded_input[:,:,pad:layer_input.shape[2]+pad, pad:layer_input.shape[3]+pad], layer_input)
        # convolve input feature maps with filters
        return conv2d(padded_input,
                        self.params[self.name+'_W'],
                        subsample=(self.stride,self.stride)) +\
                                self.params[self.name+'_b'].dimshuffle('x', 0, 'x', 'x')

    def get_shape(self, input_shape):
        """
        TODO
        """
        return [self.num_out,
                (input_shape[1] - self.filter_size[0]+ 2*self.padding)/self.stride + 1,
                (input_shape[2] - self.filter_size[1]+ 2*self.padding)/self.stride + 1]


