import numpy as np
from threading import Thread
import h5py
from threading import Thread

from ..utils import numpy_floatX

import pdb

class DataHandler(object):
    """Docstring for DataHandler. """
    def __init__(self):
        """TODO: to be defined1. """
        self.data_ready = True
        self.nonignoreclasses = None

    def _load_chunk(self, args):
        """
        TODO
        """
        raise NotImplementedError("_load_chunk should be implemented by each child")

    def request_update(self):
        def _update():
            self.data_ready = False
            print ("update is requested and it is underway")
            self.update_train()
            print("updating the data is finished")
            self.data_ready = True
        if self.data_ready == True:
            t = Thread(target=_update)
            t.start()

    def default_datainfo(self):
        """
        creates a dictinary that contains info about locataiono of data
            and the division portions
        """
        raise NotImplementedError

    def update_train(self):
        raise NotImplementedError
    def load_valid(self):
        raise NotImplementedError
    def load_test(self):
        raise NotImplementedError

