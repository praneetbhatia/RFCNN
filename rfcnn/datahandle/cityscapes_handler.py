import numpy as np
from threading import Thread
import h5py
from theano import config
from collections import namedtuple


from ..utils import numpy_floatX, sliding_window
from .datahandler import DataHandler


class CityScapesHandler(DataHandler):
    """Docstring for CityScapes dataset. """
    def __init__(self, file_path,
                 train_batchsize=16, valid_batchsize=15,
                 test_batchsize=13,
                 normalize=False, n_frames=1):
        """
        """
        super(CityScapesHandler, self).__init__()
        self.hf = h5py.File(file_path)
        self.n_frames=n_frames
        self.normalize= normalize
        self.is_temporal = self.n_frames > 1
        self.img_shape = self.hf['images_train'].shape[1:]
        self.label_shape = self.hf['labels_train'].shape[1:]
        self.n_train_samples = len(self.hf['names_train'][:])
        self.n_valid_samples = len(self.hf['names_valid'][:])
        #self.n_test_samples = len(self.hf['names_test'][:])

        self.train_batchsize = train_batchsize
        self.valid_batchsize = valid_batchsize
        #self.test_batchsize = test_batchsize

        self.compute_idxs()
        self.nonignoreclasses= [2,3,5,6,7]#None#[7,8,11,12,13,17,19,20,21,22,23,24,25,26,27,28,31,32,33]

    def compute_idxs(self):
        def get_idx(length, batchsize,reshuffle=True):
            "function to generate ids for each batch without replacement"
            pointer = 0
            shuffled = np.random.permutation(length)
            while True:
                if pointer + batchsize <= len(shuffled):
                    yield shuffled[pointer:pointer+batchsize]
                    if pointer + batchsize == len(shuffled):
                        pointer = 0
                    else:
                        pointer = pointer + batchsize
                else:
                    diff = len(shuffled) - pointer
                    yield np.concatenate((shuffled[pointer:],
                                          shuffled[0:batchsize-diff]))
                    if reshuffle:
                        shuffled = np.random.permutation(length)
                    pointer = batchsize-diff

        self.train_gen_idx = get_idx(self.n_train_samples-self.n_frames,
                                     self.train_batchsize)
        self.valid_gen_idx = get_idx(self.n_valid_samples-self.n_frames,
                                     self.valid_batchsize)
        #self.test_gen_idx = get_idx(self.n_test_samples-self.n_frames,
        #                             self.test_batchsize)

    def getMiniBatch(self, split):
        if split not in ['train', 'valid', 'test']:
            raise NameError("partion %s is not included in the dataset"%split)
        if split == 'train':
            n_samples = self.n_train_samples
            batchsize = self.train_batchsize
            gen = self.train_gen_idx
        if split == 'valid':
            n_samples = self.n_valid_samples
            batchsize = self.valid_batchsize
            gen = self.valid_gen_idx
        if split == 'test':
            n_samples = self.n_test_samples
            batchsize = self.test_batchsize
            gen = self.test_gen_idx
        n_samples = n_samples - self.n_frames + 1

        if self.is_temporal:
            X = np.zeros((batchsize, self.n_frames,
                          self.img_shape[0],
                          self.img_shape[1]* self.img_shape[2]),
                          dtype= config.floatX)
            Y = np.zeros((batchsize,
                          self.label_shape[0]*self.label_shape[1]),
                          dtype= 'int64')
        else:
            X = np.zeros((batchsize, self.img_shape[0],
                          self.img_shape[1]* self.img_shape[2]),
                          dtype= config.floatX)
            Y = np.zeros((batchsize,
                          self.label_shape[0]*self.label_shape[1]),
                          dtype= 'int64')
        idx = next(gen)
        names=[]
        correct_index= 0
        for i in range(len(idx)):
            names= self.hf['names_'+split][idx[i]:idx[i]+self.n_frames]
            tempx = np.asarray(self.hf['images_'+split][idx[i]:idx[i]+self.n_frames], dtype= config.floatX)
            X[i] = tempx.reshape((tempx.shape[0], tempx.shape[1], -1))
            if split is not 'test':
                tempy = np.asarray(self.hf['labels_'+split][idx[i]+self.n_frames-1], dtype= 'int64')
                Y[i] = tempy.flatten()

        if self.normalize :
            X= np.asarray(X/255.0, dtype=config.floatX)

        return X, Y


    def get_color(self,axis):
        cityscape_colors=[
                        (  101,  100,  100),
                        (  100,  100,  100),
                        (  100,  100,  100),
                        (  100,  100,  100),
                        (  100,  100,  100),
                        (111, 74,  0),
                        ( 81,  0, 81),
                        (128, 64,128),
                        (244, 35,232),
                        (250,170,160),
                        (230,150,140),
                        ( 70, 70, 70),
                        (102,102,156),
                        (190,153,153),
                        (180,165,180),
                        (150,100,100),
                        (150,120, 90),
                        (153,153,153),
                        (153,153,153),
                        (250,170, 30),
                        (220,220,  0),
                        (107,142, 35),
                        (152,251,152),
                        ( 70,130,180),
                        (220, 20, 60),
                        (255,  0,  0),
                        (  0,  0,142),
                        (  0,  0, 70),
                        (  0, 60,100),
                        (  0,  0, 90),
                        (  0,  0,110),
                        (  0, 80,100),
                        (  0,  0,230),
                        (119, 11, 32),
                        (  0,  0,142),]
        def f(x):
            return cityscape_colors[x][axis]
        return np.vectorize(f)

