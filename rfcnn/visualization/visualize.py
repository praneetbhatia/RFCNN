import numpy
import theano
from theano import config
import theano.tensor as T
import pdb
import scipy
import Image

class Visualization(object):
    """
    Class for visualizing the output
    """

    def __init__(self, network, size, vis_type= 'images',
                 data_handle= None, out_dir= 'output/',
                 num_classes=1, n_frames=1):
        """
        vis_type: visualization type that can be 'images' or 'video'
        data_handle: handler for the data to use its validation for visualization, if None single images that are passed will be used
        """
        self.vis_type= vis_type
        self.data_handle= data_handle
        self.out_dir= out_dir
        self.net= network
        self.size= size
        self.num_classes = num_classes
        self.n_frames = n_frames

    def generate_vis(self, images= None, gt= None):
        """
        Generates Visualization according to vis_type
        network: the network model to be used for visualization
        images: set of images that are used in the visualization, if None data_handle is expected to be used.
        """
        if self.vis_type == 'images':
            if self.data_handle is not None:
                self.generate_vis_data(self.data_handle.valid_x, self.data_handle.valid_y)
            else:
                self.generate_vis_data(images, gt)

        elif self.vis_type == 'video':
            print('Not implemented yet')


    def create_overlay(self, im, mask):
        #im= im.reshape((3,self.size[0]*2, self.size[1]*2)).transpose(1,2,0)
        #im = scipy.misc.imresize(im.transpose((1,2,0)), (self.size[0], self.size[1],3))
        im = im.transpose((1,2,0))
        orig_shape = im.shape
        im = Image.fromarray(numpy.uint8(im))
        mask= numpy.array(mask[0,:,:], dtype= 'uint8')
        mask= scipy.misc.imresize(mask, (self.size[0], self.size[1]))

        if not "get_color" in dir(self.data_handle):
            mask_r= numpy.zeros((mask.shape[1], mask.shape[2], 3))
            mask_r[:,:,0]= mask[0,:,:]
            if not mask_r.shape == orig_shape:
                mask_r = scipy.misc.imresize(mask_r, orig_shape)
            overlay = Image.fromarray(numpy.uint8(mask_r*(255//self.num_classes)))
            overlay = overlay.convert("RGBA")
        else:
#            f0 = self.data_handle.get_color(0)
#            f1 = self.data_handle.get_color(1)
#            f2 = self.data_handle.get_color(2)
#            mask_r = numpy.concatenate((f0(mask), f1(mask), f2(mask))).transpose(1,2,0)
            colors= [(0,0,255), (255,0,0), (0, 128, 0), (255,255,0), (0,255,255)]
            mask_r= numpy.zeros((mask.shape[0], mask.shape[1],3), dtype= numpy.uint8)
            mask_gray= numpy.zeros((mask.shape[0], mask.shape[1]), dtype= numpy.uint8)
            interest= [2,3,5,6,7]
            for i in range(0,len(interest)):
                for j in range(3):
                    mask_r[(mask==interest[i]),j]= colors[i][j]
            overlay = Image.fromarray(numpy.uint8(mask_r))
            overlay = overlay.convert("RGBA")

        im = im.convert("RGBA")
        im = Image.blend(im, overlay, 0.4)
        return im

    def generate_vis_data(self, X, Y):
        """
        Generate Visualization for all validation data in self.data_handle
        X: data subset for evaluation
        Y: labels for X
        batch_size: the batch_size used for evaluation
        """
        self.net.set_phase('TEST')

        for i in range(self.n_frames, X.shape[0]):
            print("{} images out of {} are processed".format(i, X.shape[0]))
            if self.n_frames == 1:
                im= X[i,:, :, :].reshape(1, X.shape[1],X.shape[2]* X.shape[3])
            if self.n_frames > 1:
                im = X[i-self.n_frames+1:i+1,:, :, :].reshape(1,self.n_frames,X.shape[1],X.shape[2]*X.shape[3])
            pred = self.net.f_pred(im)
            #im= X[i,-1, :, :].reshape(1,X.shape[2], X.shape[3])

            scipy.misc.imsave(self.out_dir+'pred/'+str(i)+'.png' ,numpy.asarray(pred.reshape((self.size[0]//2, self.size[1]//2)),dtype='uint8') )
            im= self.create_overlay(X[i,:, :, :], pred.reshape((1,self.size[0]//2, self.size[1]//2)))
            im.save(self.out_dir+'images/'+str(i)+'.png', 'PNG')

            scipy.misc.imsave(self.out_dir+'gt/'+str(i)+'.png' ,Y[i, :] )

        self.net.set_phase('TRAIN')
