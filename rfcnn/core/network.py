import numpy
from collections import OrderedDict as OD
import theano
from theano import config
import theano.tensor as T
import inspect
import pdb


class Network(object):
    """
    Contains a model of the network.
    compiles the layers.
    shapes functionalities.
    creates theano functions for running the network
    """
    def __init__(self, name="", output_type='segmentation'):
        self.name = name
        self.layers = OD()
        self.params = OD()
        self.shapes = OD()
        self.comp_graphs = OD()
        self.output_type = output_type
        self.x = None
        self.y = None


    def add_layer(self, layer):
        """
        appends a layer object to the list
        layer: a layer object
        """
        self.layers[layer.name] = layer

    def init_input(self, x_ndim, y_ndim):
        """
        initilzing theano tensor objects.
        x_ndim: dimension of the input data
        y_ndim: dimension of the labels data
        """
        if x_ndim == 2:
            self.x = T.matrix('x')
        elif x_ndim == 3:
            self.x = T.tensor3('x')
        elif x_ndim == 4:
            self.x = T.tensor4('x')
        else:
            print ('Wrong input dimension!')

        if y_ndim == 1:
            self.y = T.lvector('y')
        elif y_ndim == 2:
            self.y = T.lmatrix('y')
        else:
            print ('Wrong label dimension!')

    #def init_input_temp(self, x_ndim, y_ndim):
    #    self.x = TensorType(config.floatX, (False,)*x_ndim)
    #    self.y = TensorType(config.floatX, (False,)*y_ndim)

    def get_shape(self, input_shape=None):
        """
        loops over all layers and compute the shapes of the output of each
            based on the implemenation of the layer.
        fills self.shapes dictionary with the results.
        returns last computed shape.
        input_shape: shape of the data given to the first layer.
                     can be None if the first layer is a data layer
                     with reshaping to the actual size inside.
        """
        for name, layer in self.layers.items():
            if hasattr(layer, 'params'):
                self.params = OD (self.params.items() + layer.params.items())
            if hasattr(layer, 'bottom'):
                input_shape = layer.get_shape(self.shapes)
            else:
                input_shape = layer.get_shape(input_shape)
            self.shapes[name] = input_shape
        return input_shape

    def get_lradj(self):
        """
        collects learning rate adjustment from each layer
            (and the network inside each if they have one).
        returns a dictionary of all params and their learning rate adjustmens.
        :returns: TODO
        """
        lr_adj = OD()
        for layer in self.layers.values():
            if hasattr(layer, 'lr_adj'):
                for k,v in layer.params.items():
                    lr_adj[k] = layer.lr_adj
            if hasattr(layer, 'net'):
                lr_adj = OD(layer.net.get_lradj().items() + lr_adj.items())
        return lr_adj


    def set_phase(self, phase):
        for k, v in self.layers.items():
            if 'drop' in k:
                self.layers[k].phase= phase

    def compile(self, layer_input=None, label_input=None ):
        """
        compiles the computational graph.
        starts with the layer_input and goes over each layer.
        if the net includes a layer which needs labels label_input
            should also be provided.
        if either of layer_input or label_input is none, the function
            will use self.x, self.y respectivley.
        Fills up self.comp_graphs with the graph after each layer.
        returns the last graph
        """
        if layer_input == None and self.x is None:
            raise AttributeError("no layer_input is given and network variables are not initilaized")
        if layer_input is None:
            layer_input = self.x


        input_shape = None
        for name, layer in self.layers.items():
            if hasattr(layer, 'params'):
                self.params = OD (self.params.items() + layer.params.items())
            if hasattr(layer, 'bottom'):
                input_shape = layer.get_shape(self.shapes)
            else:
                input_shape = layer.get_shape(input_shape)
            self.shapes[name] = input_shape


        for name, layer in self.layers.items():
            #print('layer ', name)
            if hasattr(layer, 'bottom'):
                layer_input = layer(self.comp_graphs)
            else:
                if len(inspect.getargspec(layer.__call__).args) == 2:
                    layer_input = layer(layer_input)
                elif len(inspect.getargspec(layer.__call__).args) == 3:
                    if label_input == None and self.y is None:
                        raise AttributeError("no label_input is given and network variables are not initilaized")
                    if label_input == None:
                        label_input = self.y
                    self.forward_model = layer_input
                    layer_input = layer(layer_input, label_input)
            self.comp_graphs[name] = layer_input

        return layer_input

    def get_grads(self, cost_layer_name='cost'):
        """
        computes the gradinets of params w.r.t the cost_layer_name
        first creates a complete list of all params in the network.
            (if a layer has a network inside, this function will also include
            params from there. be careful to not have repeated names there.)
        returns the gradients.
        """
        for name, layer in self.layers.items():
            if hasattr(layer, 'net'):
                self.params = OD(self.params.items() + layer.net.params.items())
        return T.grad(self.comp_graphs[cost_layer_name], wrt=list(self.params.values()))

    def create_functions(self, x_ndim, y_ndim):
        """
        initializes the input variables.
        compiles the model.
        computes the gradients
        call the optimizer.
        creates theano functions for the network:
            f_forward: forwards the data to the network upto the last layer before cost
            f_pred: computes the predictions.
            f_forward_grad: from optimizer. forward through the whole network and updates the grads
            f_update: from the optimzer. does the weight update based on the otimizer's policy.
        """
        self.init_input(x_ndim, y_ndim)
        self.compile()
        self.grads = self.get_grads()

        # in case of classification predict the max prob class
        if self.output_type == 'classification':
            self.y_pred = T.argmax(self.comp_graphs['prob'], axis=1)
        elif self.output_type == 'segmentation':
            y_pred = T.argmax(self.comp_graphs['prob'], axis=1)
            self.y_pred = y_pred.reshape((self.x.shape[0], -1))

        #shp= self.y_pred.shape
        self.test_model = T.mean(T.neq(self.y_pred, self.y))

        self.f_forward= theano.function([self.x], self.forward_model)
        self.f_pred = theano.function([self.x], self.y_pred)

        lr= T.scalar('lr')
        lr_adj = self.get_lradj()
        self.f_forward_grad, self.f_update= self.optimizer.get_update_func(
                                                        lr_adj, self.params,
                                                        self.grads, self.x, self.y,
                                                        self.comp_graphs['cost'])

        self.f_test = theano.function([self.x, self.y], self.test_model)
        self.f_cost = theano.function([self.x, self.y], self.comp_graphs.values()[-1])


