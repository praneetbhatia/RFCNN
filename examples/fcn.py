import gzip
import numpy
import matplotlib.pyplot as plt
from theano import config
import argparse

import os
filepath = os.path.dirname(os.path.abspath(__file__))
rootpath = os.path.abspath(os.path.join(filepath, os.pardir))
os.sys.path.insert(0, filepath)
os.sys.path.insert(0, rootpath)

from rfcnn.core import Network
from rfcnn.core import SGD, RMSPROP, ADADELTA
from rfcnn.utils import numpy_floatX, load_all_datasets, load_weights
from rfcnn.layers import Conv, Deconv, Pooling, Relu, Data, Softmax, Sigmoid, NLL, TransDeconv, Concat, Skip
from rfcnn.core import TrainByBatch
from rfcnn.datahandle import SynthiaHandler

import pdb
nclasses= 14
orig_size= [3, 190, 320]

def prep_network():
    # Create the network layers
    net = Network(name='fcn')

    net.add_layer(Data('data', orig_size, reshape= True))
    net.add_layer(Conv('conv1',5,3,3,20, padding=50))
    net.add_layer(Relu('relu1'))
    net.add_layer(Pooling('pool1'))

    net.add_layer(Conv('conv2',5,1,20,50, padding=2))
    net.add_layer(Relu('relu2'))
#    net.add_layer(SpatialDropout('drop1', prob= 0.5, phase= 'TRAIN'))
    net.add_layer(Pooling('pool2'))

    net.add_layer(Conv('conv3',3,1,50,500, padding=1))
    net.add_layer(Relu('relu3'))
#    net.add_layer(SpatialDropout('drop2', prob= 0.5, phase= 'TRAIN'))
    #net.add_layer(Concat('concat1', ['conv3', 'relu3']))

    net.add_layer(Conv('conv4',1,1,500,nclasses))

    net.add_layer(Skip('skip1', ['conv4', 'pool1'], [2,1]))
    net.add_layer(TransDeconv('deconv1', 10, 12, 34, nclasses, out_crop_size = (orig_size[1], orig_size[2])))

    net.add_layer(Softmax('prob'))
    net.add_layer(NLL('cost', 'segmentation'))

    net.optimizer = ADADELTA()
    net.create_functions(3, 2)
    train_configs = {
                'max_epochs' : 500,  # The maximum number of epoch to run
                'valid_freq' : 10,  # Compute the validation error after this number of update.
                'train_batch_size':10,  # The batch size during training.
                #'net_save_path': "/usr/data/menna/RFCNN_data/models/fcn.save",
                #'log_file' : '/usr/data/menna/RFCNN_data/logs/fcn.txt',
                }
    directory= '/usr/data/Datasets/Synthia/synthia_SEQ_02_RAIN_splits.h5'
    data = SynthiaHandler(directory, normalize=True)
    train = TrainByBatch(net, datahandler=data,
                  configs=train_configs, nclasses= nclasses)
    return train

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', help="choose either test or train. if test, provide the path to the model (--model)\n \
                        add train along with a model to continue training.")
    parser.add_argument('--model', help="path to the model(.save)")
    args = parser.parse_args()

    train = prep_network()
    if args.mode == 'train':
        if args.model is not None:
            print ("continue training from %s"%(args.model))
            load_weights(train.net, args.model)
        train.run_all()

    elif args.mode == 'test':
        if args.model is None:
            raise NameError("no model is passed for testing")

        load_weights(train.net, args.model)
        cost, prec, recall, fmeasure = train.eval(valid_set_x, valid_set_y)
        print ("cost = %s, precision = %s, recall = %s, fmeasure =%s"%(
               cost, prec, recall, fmeasure))

if __name__ == '__main__':
        main()
