import gzip
import numpy
import matplotlib.pyplot as plt
from theano import config
import argparse
import pdb

import os

filepath = os.path.dirname(os.path.abspath(__file__))
rootpath = os.path.abspath(os.path.join(filepath, os.pardir))
os.sys.path.insert(0, filepath)
os.sys.path.insert(0, rootpath)
from rfcnn.core import Network
from rfcnn.core import SGD, RMSPROP, ADADELTA
from rfcnn.utils import numpy_floatX, load_all_datasets, load_weights, copy_caffe_weights
from rfcnn.layers import Conv, Deconv, Pooling, Relu, Data, Sigmoid, NLL, LRN, TransDeconv, SpatialDropout
from rfcnn.core import Train
from rfcnn.datahandle import SegTrackHandler

directory= '/usr/data/Datasets/SegTrackv2/data_2/'
CHD = SegTrackHandler(directory, update_train=False)
orig_size= [3, 240, 360]

def prep_network():
    # Create the network layers
    net = Network(name='fcn')

    net.add_layer(Data('data', orig_size, reshape=True, mean_subtraction=(102.7170, 115.7726, 123.5094)))

    net.add_layer(Conv('conv1',11,4,3,64, padding=40, lr_adj=0.))
    net.add_layer(Relu('relu1'))
    net.add_layer(Pooling('pool1'))
    net.add_layer(LRN('LRN1'))

    net.add_layer(Conv('conv2',5,1,64,256,padding=2, lr_adj=0.))
    net.add_layer(Relu('relu2'))
    net.add_layer(Pooling('pool2'))

    net.add_layer(Conv('conv3',3,1,256,256, padding=1, lr_adj=0.))
    net.add_layer(Relu('relu3'))

    net.add_layer(Conv('conv4',3,1,256,256, padding=1, lr_adj=0.))
    net.add_layer(Relu('relu4'))

    net.add_layer(Conv('conv5',3,1,256,256, padding=1, lr_adj=0.))
    net.add_layer(Relu('relu5'))

    net.add_layer(Conv('conv6',1,1,256,512))
    net.add_layer(Relu('relu6'))
    net.add_layer(SpatialDropout('drop1', prob= 0.5))

    net.add_layer(Conv('conv7',1,1,512,128))
    net.add_layer(Relu('relu7'))
    net.add_layer(SpatialDropout('drop2', prob= 0.5))

    net.add_layer(Conv('conv8',3,1,128,128, padding=3))
    net.add_layer(Conv('conv9',1,1,128,1))
#    net.add_layer(Relu('relu8'))

    net.add_layer(TransDeconv('deconv1', 20, 8, 1, 1, out_crop_size = (orig_size[1]//2, orig_size[2]//2)))

    net.add_layer(Sigmoid('prob'))
    net.add_layer(NLL('cost', 'segmentation'))

    copy_caffe_weights(net, "/usr/data/RFCNN_data/VGG_Models/VGG_CNN_F.caffemodel",
                            "/usr/data/RFCNN_data/VGG_Models/VGG_deploy.prototxt")

    net.optimizer = ADADELTA()
    net.create_functions(3, 2)
    train_configs = {
                'max_epochs' : 500,  # The maximum number of epoch to run
                'patience' : 80,  # Number of epoch to wait before early stop if no progress
                'valid_freq' : 10,  # Compute the validation error after this number of update.
                'patience' : 150,  # Number of epoch to wait before early stop if no progress
                'save_freq': 10,
                'train_batch_size': 10,  # The batch size during training.
                'epochs_per_datachunk':3, #Defines number of epochs that the training spends on ecach data chunk
                'net_save_path': "/usr/data//RFCNN_data/models/vgg_fcn_segtrack_extraconv.save",
                'log_file' : '/usr/data/RFCNN_data/logs/vgg_fcn_segtrack_extraconv.txt',
                }

    train = Train(net, X=CHD.train_x, Y=CHD.train_y,
                  X_valid=CHD.valid_x, Y_valid=CHD.valid_y,
                  configs=train_configs)
    return train

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', help="choose either test or train. if test, provide the path to the model (--model)\n \
                        add train along with a model to continue training.")
    parser.add_argument('--model', help="path to the model(.save)")
    parser.add_argument('--interm', help="whether save intermediate output or not")
    args = parser.parse_args()

    train = prep_network()
    if args.mode == 'train':
        train.net.set_phase('TRAIN')
        if args.model is not None:
            print ("continue training from %s"%(args.model))
            load_weights(train.net, args.model)
        train.run_all()

    elif args.mode == 'test':
        if args.model is None:
            raise NameError("no model is passed for testing")
        train.net.set_phase('TEST')
        load_weights(train.net, args.model)
        met,_ = train.eval(CHD.valid_x, CHD.valid_y)
        print ("cost = %s, precision = %s, recall = %s, fmeasure =%s, IOU= %s"%(
               met.cost, met.prec, met.rec, met.fmes, met.mean_iou_index))

        if args.interm== 'on':
            _, train_interm_x= train.eval(train_set_x, train_set_y, save_interm= 'conv8')
            train_interm_x = train_interm_x.reshape(-1,train_interm_x.shape[1],train_interm_x.shape[2]*train_interm_x.shape[3])

            _, valid_interm_x= train.eval(valid_set_x, valid_set_y, save_interm= 'conv8')
            valid_interm_x = valid_interm_x.reshape(-1,valid_interm_x.shape[1],valid_interm_x.shape[2]*valid_interm_x.shape[3])

            _, test_interm_x= train.eval(test_set_x, test_set_y, save_interm= 'conv8')
            test_interm_x = test_interm_x.reshape(-1,test_interm_x.shape[1],test_interm_x.shape[2]*test_interm_x.shape[3])
            numpy.save('/usr/data/menna/Datasets/change_detection_datasets/numpy_data/pedestrians_conv.npy' ,
                    ((train_interm_x, train_set_y), (valid_interm_x, valid_set_y), (test_set_x, test_set_y)))

if __name__ == '__main__':
        main()
